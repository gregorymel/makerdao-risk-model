#%%
import pandas as pd
import pickle
import base64
import cbpro
from datetime import datetime, timedelta, timezone
from tqdm import tqdm
import bisect
import ast
from numpy.polynomial import Polynomial
from scipy import optimize
from scipy.special import binom
import numpy as np
from functools import reduce
import math

TIME_FORMAT = '%Y-%m-%d %H:%M:%S UTC'
FLIP_ADDR_1 = '0xd8a04F5412223F513DC55F839574430f5EC15531'
FLIP_ADDR_2 = '0xF32836B9E1f47a0515c6Ec431592D5EbC276407f'
TIME_FACTOR = 60 #minutes

class EthDaiRate(object):
    def __init__(self, rates):
        self.rates = rates
        self.time = [row[0] for row in rates]

    def find_lt(self, pivot):
        i = bisect.bisect_left(self.time, pivot)

        return self.rates[i][1]

def get_price_history():
    time = datetime.strptime("2019-11-12 00:00:00 UTC", TIME_FORMAT)
    end = datetime.strptime("2021-04-20 00:00:00 UTC", TIME_FORMAT)
    delta = timedelta(hours=4)

    client = cbpro.PublicClient()

    result = []
    while time < end:
        res = client.get_product_historic_rates(
            'ETH-DAI', 
            start=time, 
            end=time + delta, 
            granularity=60)

        result.extend(
            map(lambda x: (x[0], x[1]), res)
        )

        time += delta

    with open('eth-dai-all.pkl', 'wb+') as f:
        pickle.dump(result, f)

def get_utc_timestamp(time):
    return datetime.strptime(time, TIME_FORMAT).replace(tzinfo=timezone.utc)


## indecies of df should be reseted
def cluster_by_ops(df):
    prev_fn = None

    grab_flux_ops = []
    frob_fork_ops = []
    prev_frob_fork_ops = []

    for index, row in df.iterrows():
        fn = row['fn_name']
        src = row['src']
    
        if fn == 'grab':
            if (prev_fn == 'grab' or prev_fn == 'flux'):
                grab_flux_ops.append(row)
                continue

            if grab_flux_ops:
                yield (prev_frob_fork_ops, grab_flux_ops)

            grab_flux_ops = []
            prev_frob_fork_ops = frob_fork_ops
            frob_fork_ops = []

            grab_flux_ops.append(row)

        elif fn == 'flux':
            grab_flux_ops.append(row)

        else:
            frob_fork_ops.append(row)

        prev_fn = fn

    yield (prev_frob_fork_ops, grab_flux_ops)
    if frob_fork_ops:
        yield (frob_fork_ops, [])

def handle_grab_flux(grab_flux_ops, eth_dai_rate):
    dink_dai = 0
    flux_dai = 0
    dart_dai = 0
    total_dart = 0

    for row in grab_flux_ops:
        fn_name = row['fn_name']
        time = get_utc_timestamp(row['timestamp']).timestamp()

        if fn_name == 'grab':
            fold_rate = row['rate'] / 1e27
            dart = row['dart'] / 1e18
            total_dart += dart

            dtab = dart * fold_rate
            dart_dai += -dtab

            dink = -1 * row['dink'] / 1e18
            dink_dai += (dink * eth_dai_rate.find_lt(time))

        if fn_name == 'flux':
            wad = row['wad'] / 1e18
            ex_rate = eth_dai_rate.find_lt(time)
            flux_dai += (wad * ex_rate)

    lgd = (dink_dai - flux_dai) - dart_dai

    return lgd, total_dart

def handle_frob_fork(frob_fork_ops):
    total_dart = 0
    prev_time = get_utc_timestamp(frob_fork_ops[0]['timestamp']).timestamp()
    params = []

    for row in frob_fork_ops:
        time = get_utc_timestamp(row['timestamp']).timestamp()
        dtime = (time - prev_time) / TIME_FACTOR
        prev_time = time

        fold_rate = row['rate'] / 1e27
        dart = row['dart'] / 1e18

        total_dart += dart

        dtab = dart * fold_rate

        params.append((dtab, dtime))

    return params, total_dart

def calculate_interest(params, init_tab):
    def p_or(x):
        total = init_tab
        for dtab, dt in params:
            total = (total * ((1 + x) ** dt) + dtab)

        return total

    def p(F, scale_factor):
        total = init_tab
        for dtab, dt in params:
            dT = dt / scale_factor
            total = (total * (F ** dT) + dtab)

        return total

    scale_factor = 20.0
    F = optimize.newton(lambda x: p(x, scale_factor), 1.0, maxiter=1000, tol=1e-10)
    r = 2 ** (np.log2(F) / scale_factor) - 1

    p_or_r = p_or(r)
    if abs(p_or_r) > 1e-4:
        scale_factor = 30.0
        F = optimize.newton(lambda x: p(x, scale_factor), 1.1, maxiter=1000, tol=1e-10)
        r = 2 ** (np.log2(F) / scale_factor) - 1

    return r


def split_frob_fork_ops(fork_frob_ops, init_art=0.0):
    def sum(acc, x):
        val = acc[-1] + x[1]['dart']
        acc.append(val)

        return acc
    
    vals = reduce(sum, fork_frob_ops.iterrows(), [0,])
    zeros = np.isclose(vals[1:], 0, atol=10, rtol=0)
    zeros = np.where(zeros == True)
    zeros = [x for x in zeros[0]]
    zeros.append(-1)

    loans = []
    prev_pos = 0
    index = ['usr', 'start', 'end', 'status'] ##, 'interest rate', 'LGD']
    for pos in zeros:
        loan = None
        status = None
        if pos != -1:
            loan = fork_frob_ops.iloc[prev_pos:pos+1]
            status = 'finished'
        else:
            loan = fork_frob_ops.iloc[prev_pos:]
            status = 'active'
            if len(loan) == 0:
                continue

        prev_pos = pos+1

        start = loan.iloc[0]['timestamp']
        end = loan.iloc[-1]['timestamp']
        usr = loan.iloc[0]['usr']
        data=[usr, start, end, status]
        loans.append(pd.Series(data=data, index=index))

    return loans

def get_results(df, eth_dai_rate):
    art = 0
    for frob_fork_ops, grab_flux_ops in cluster_by_ops(df):
        index = ['usr', 'start', 'end', 'status', 'interest rate', 'LGD']
        start = frob_fork_ops[0]['timestamp']
        vault = frob_fork_ops[0]['usr']
        end = None

        if not grab_flux_ops:
            data = [vault, start, end, 'active', None, None]
            yield pd.Series(data=data, index=index)
            continue

        init_rate = frob_fork_ops[0]['rate'] / 1e27
        init_tab = art * init_rate

        params, dart_ff = handle_frob_fork(frob_fork_ops)
        lgd, dart_gf = handle_grab_flux(grab_flux_ops, eth_dai_rate)

        art += dart_ff
        
        last_time_ff = get_utc_timestamp(frob_fork_ops[-1]['timestamp']).timestamp()
        first_time_gf = get_utc_timestamp(grab_flux_ops[0]['timestamp']).timestamp()
        dtime = (first_time_gf - last_time_ff) / TIME_FACTOR

        rate = grab_flux_ops[0]['rate'] / 1e27
        tab = art * rate

        params.append((-(tab + lgd), dtime))
        r = calculate_interest(params, init_tab)

        end = grab_flux_ops[-1]['timestamp']
        data = [vault, start, end, 'default', r, lgd]
        yield pd.Series(data=data, index=index)

        art += dart_gf

def filter_ethA_data(ethA):
    ethA_filtered = ethA[
        (ethA['fn_name'] == 'grab')
        |
        (
            (ethA['fn_name'] == 'flux')
            &
            (
                (ethA['src'] == FLIP_ADDR_1)
                |
                (ethA['src'] == FLIP_ADDR_2)
            )
        )
        |
        (
            (ethA['fn_name'] == 'frob')
            &
            (ethA['dart'] != 0.0)
        )
        |
        (
            (ethA['fn_name'] == 'fork')
            &
            (ethA['dart'] != 0.0)
        )
    ]

    ethA_filtered = ethA_filtered.astype({ 'rate': 'float' })

    fork_df = ethA_filtered[
        ethA_filtered['fn_name'] == 'fork'
    ]

    def decode(row):
        row['usr'] = row['src']
        row['src'] = None

        row['dart'] = -row['dart']

        return row

    fork_df_inv = fork_df.apply(decode, axis=1)

    return pd.concat([ethA_filtered, fork_df_inv], axis=0).sort_index()

# def get_statistics(df, eth_dai_rate):
#     # fold_rate = 10 ** 27
#     dink_dai = 0
#     flux_dai = 0
#     dart_dai = 0

#     params = []
#     pos = 0
#     params.append([])
#     # p(x) = a * x + b
#     # a = 0.0
#     # b = 0.0

#     final_op = df.iloc[-1]['fn_name']
#     prev_time = get_utc_timestamp(df.iloc[0]['timestamp']).timestamp()

#     art = 0.0
#     # final_tab = 0.0
#     # final_rate = df.iloc[-1]['rate'] / 1e27

#     for index, row in df.iterrows():
#         fn_name = row['fn_name']
#         fold_rate = row['rate']
#         time = get_utc_timestamp(row['timestamp']).timestamp()
#         # dtime = final_time - time
#         dtime = (time - prev_time) / 60
#         prev_time = time

#         if fn_name == 'grab':
#             dart = row['dart'] / 1e18
            
#             dtab = dart * (fold_rate / 1e27)
#             dart_dai += -dtab

#             dink = -1 * row['dink'] / 1e18
#             dink_dai += (dink * eth_dai_rate.find_lt(time))

#             # a += dtab * dtime
#             # b += dtab
#             final_tab = art * (fold_rate / 1e27)
#             params[pos].append((-final_tab, dtime))
#             art += dart

#             pos += 1
#             params.append([])
#             tab = art * (fold_rate / 1e27)
#             params[pos].append((tab, 0.0))


#         if fn_name == 'flux':
#             ex_rate = eth_dai_rate.find_lt(time)
#             flux_dai += ((row['wad'] / 1e18) * ex_rate)

#         if fn_name == 'frob' or fn_name == 'fork':
#             dart = row['dart'] / 1e18
#             art += dart

#             dtab = dart * (fold_rate / 1e27)

#             # a += dtab * dtime
#             # b += dtab
#             params[pos].append((dtab, dtime))

#     final_tab = 0.0
#     if final_op != 'grab':
#         final_tab = art * (fold_rate / 1e27)
    
#     lgd = (dink_dai - flux_dai) - dart_dai

#     # x = 0.0

#     # print (params)
#     # print ('tab', final_tab, art, (fold_rate / 1e27))

#     def power_series(x, k, N):
#         res = 0.0
#         for i in range(k):
#             coeff = binom(N, i)
#             res += coeff * (x ** i)

#         return res

#     def p(x):
#         # print ("x", x)
#         result = 0.0
#         for i in range(len(params)):
#             total = 0.0
#             for dtab, dt in params[i]:
#                 # print (dtab, dt)
#                 # total = (total * power_series(x, 2, dt) + dtab)
#                 # print ('{} * ((1 + x) ** {}) + {}'.format(total, dt, dtab))
#                 total = (total * ((1 + x) ** dt) + dtab)

        
#             result += total

#         return result - lgd - final_tab

#     # def f(F):
#     #     # print ("x", x)
#     #     result = 0.0
#     #     for i in range(len(params)):
#     #         total = 0.0
#     #         for dtab, dt in params[i]:
#     #             total = (total * (2 ** (dt * F)) + dtab)

        
#     #         result += total

#     #     return result - lgd - final_tab

#     x = optimize.newton(p, 0.0, maxiter=1000, tol=1e-10)

#     # print ('f(x)', f(x))

#     # x = (tab + lgd - b) / a

#     return x, lgd

# %%
eth_dai_rates = None
file_name = "ethA-dai-price.csv"
with open(file_name, 'rb+') as f:
    eth_dai_rates = pickle.load(f)

#%%
rate = EthDaiRate(eth_dai_rates)

# %%
file_name = "maker_vat_calls_ethA_2019-2021.csv"
ethA = pd.read_csv(file_name)

ethA_default = filter_ethA_data(ethA)

# %%
grouped = ethA_default.groupby('usr')
grab = ethA[ethA['fn_name'] == 'grab']
grab_usr = set(grab.groupby('usr').groups.keys())
other_usr = set(grouped.groups.keys()) - grab_usr

# get credits of users who weren't liquidated
loans_all = []
for v in tqdm(other_usr):
    v_gr = grouped.get_group(v)
    loans = split_frob_fork_ops(v_gr)
    
    loans_all.extend(loans)

pd.DataFrame(loans_all).to_csv('finished_credits.csv')

#%%
stat = []

# get credits of users who were liquidated at least once
for vault in tqdm(grab_usr):
    group = grouped.get_group(vault)

    stat.extend(
        [row for row in get_results(group, rate)]
    )

# %%
loans = pd.DataFrame(stat)
#%%
loans[loans['LGD'] > 0.0].to_csv('credits.csv')