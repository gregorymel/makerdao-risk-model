SELECT
        block_timestamp,
        block_number,
        transaction_index,
        trace_address,
        transaction_hash,
        input,
        ARRAY_TO_STRING(
                ARRAY(
                        SELECT CHR(CAST(num AS INT64)) FROM UNNEST(SPLIT(trace_address, ',')) AS num
                ),
                ","
        ) as trace_addr_str
FROM  `bigquery-public-data.crypto_ethereum.traces`
WHERE DATE(block_timestamp) >= "2019-11-12"
        and trace_address IS NOT NULL
        and call_type = "call"
        and to_address = LOWER("0x35D1b3F3D7966A1DFe207aa4514C12a259A0492B")
        and status = 1
ORDER BY CAST(block_number as INT64), CAST(transaction_index as INT64), trace_addr_str